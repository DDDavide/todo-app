console.log('file js caricato');

//codice che aggiunge il todo in db via http[post]
async function addTodo() {


    var inputEl = document.getElementById("addTodoButton");
    var description = inputEl.value; //recupero del valore inserito nella casella di testo del

    var fetchConfig = {
        method: 'POST',  //configurazione fetch con metodo post
        headers:
            { 'Content-Type': 'application/json' },
            
        body: JSON.stringify({ //serializzazione dell'oggetto 
                description: description  //passo un oggetto con la proprieta description che contiene il todo
            })
    };

    await fetch('/api/todos', fetchConfig);

}

async function main() {
    console.log('documento caricato completamente');

    //chiamata http verso /api/todos
    //await dice al broswer di aspettare il risultato della fetch
    var response = await fetch('/api/todos');
    var todos = await response.json();
    var appDiv = document.getElementById('app'); //prendo il tag div
    var ulElement = document.createElement('ul'); //creo il tag ul
    var inputEl = document.createElement('input'); //tag input per scrivere nuovo todo
    inputEl.type = "text"; //tipo input testo 
    inputEl.id = "addTodoButton"; //aggiunta id
    appDiv.appendChild(inputEl); //metto la casella di testo nell'html
    var buttonEl = document.createElement('button'); //bottone per invio
    buttonEl.innerText = "Aggiungi  "; //testo all'interno del bottone
    appDiv.appendChild(buttonEl); //metto il bottone nell'hmtl

    buttonEl.addEventListener("click", addTodo);

    appDiv.appendChild(ulElement);

    for (var i = 0; i < todos.length; i++) {
        var todo = todos[i];
        var liElement = document.createElement('li');
        liElement.innerText = todo.description;
        ulElement.appendChild(liElement);
    }

    console.log(todos);

}

//aspetto che la pagina sia stata caricata per intero
document.addEventListener('DOMContentLoaded', main);