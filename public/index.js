//Recupera dei dati in formato json a un url
//(Asincrona) per dichiararla si usa async
const fetchJSON = async (url) => {
    //Oggetto risposta
    //Se solo un parametro (l' url della risorsa), la richiesta invitata di default 
    //è di tipo GET
    let resp = await fetch(url);
    //Ritorno i dati come json
    return await resp.json();
    //creo un nuova funzione che crea il parametro id bindato in modo da richiamare la funzione deleteTodo con quell'id fisso
};

//Visualizzo i to-do 
const updateTODOs = async () => {
    //Recupero i task con l'api
    let todos = await fetchJSON("/api/todos");
    //Ottengo dal DOM la lista dei to-do
    let list = document.getElementById("tasks");
    list.innerHTML = "";
    for (let i = 0; i < todos.length; i++) {
        //Crea un elemento
        let bullet = document.createElement("li");
        /*
        <li></li>
        */
        //bullet.className = "task";
        let desc = document.createElement("div");
        /*
        <div></div>
        */
        desc.textContent = todos[i].description;
        /*
        <div>
            ciao sono la descizione del to-do
        </div>
        */
        //desc.className = "description";
        bullet.appendChild(desc);
        /*
         <li>
            <div>
            ciao sono la descizione del to-do
            </div>
         </li>
        */
        let insertD = document.createElement("div");
         /*
        <div></div>
        */
        //insertD.className = "insertDate";
        insertD.textContent = todos[i].insertDate;
         /*
        <div>
            La data di inserimento lol (tipo 20/02/22)
        </div>
        */
        bullet.appendChild(insertD);
        /*
        <li>
            <div>
            ciao sono la descizione del to-do
            </div>
            <div>
            La data di inserimento lol (tipo 20/02/22)
            </div>
         </li>
        */
        let done = document.createElement("input");
        /*
        <input >
        */
        done.type = "checkbox";
        /*
        <input type="checkbox">
        */
        done.checked = todos[i].done;
        /*
        <input type="checkbox">
        */
        //done.className = "t-done";
        //Ascolto per dei cambiamenti
        //Primo parametro nome dell'evento
        //Secondo callback per eseguire qualcosa con il cambiamento, in questo caso update
        done.addEventListener("change", () => {
            updateTODO(todos[i].id, todos[i].description, done.checked);
        });
        bullet.appendChild(done);
        /*
        <li>
            <div>
            ciao sono la descizione del to-do
            </div>
            <div>
            La data di inserimento lol (tipo 20/02/22)
            </div>
            <input type="checkbox">
         </li>
        */
        //Tasto per eliminare la task
        let del = document.createElement("button");
        /*
        <button></button>
        */
        //del.className = "del-btn";
        //Ascolto per gli eveti click (è un bottone)
        //E nel callback metto la funzione per eliminare il task
        del.addEventListener("click", () => {
            deleteTODO(todos[i].id);
        });
        del.textContent = "❌";
        /*
        <button>
        ❌
        </button>
        */
        bullet.appendChild(del);
        /*
        /*
        <li>
            <div>
            ciao sono la descizione del to-do
            </div>
            <div>
            La data di inserimento lol (tipo 20/02/22)
            </div>
            <input type="checkbox">
            <button>
            ❌
            </button>
         </li>
        */
        list.appendChild(bullet);
        /*
        <ul>
            <li>
                <div>
                ciao sono la descizione del to-do
                </div>
                <div>
                La data di inserimento lol (tipo 20/02/22)
                </div>
                <input type="checkbox">
                <button>
                ❌
                </button>
            </li>
        </ul>
        */   
    }
};

//Elimino il task
const deleteTODO = async (id) => {
    //Primo parametro url
    //Secondo url oggetto con opzioni, in questo caso method per segnalare il metodo da usare
    let resp = await fetch(`/api/todos/${id}`, {
        method: "DELETE"
    });
    //resp.status è il codice di stato http ritornato
    if (resp.status !== 204) {
        alert("Si è verificato un errore durante l'eliminazione");
    }
    updateTODOs();
};

//Aggiorno il task
const updateTODO = async (id, desc, done) => {
    let resp = await fetch(`/api/todos/${id}`, {
        method: "PUT",
        body: JSON.stringify({
            description: desc,
            done: done
        }),
        //Header per indicare il tipo di contenuto, per indicare
        //A lumen di interpretare il body come json, senò non 
        //sa che cazzo farsene 
        headers: {
            "Content-Type": "application/json"
        }
    });
    //Se il codice di risposta non è quello che ho messo
    //in lumen qualcosa è andato storto
    if (resp.status !== 200) {
        alert("Si è verificato un errore durante l'aggiornamento");
    }
    //Aggiorno la lista
    updateTODOs();

//Inserisco il task
const insertTODO = async (desc) => {
    let resp = await fetch(`/api/todos`, {
        method: "POST",
        body: JSON.stringify({
            description: desc
        }),
        headers: {
            "Content-Type": "application/json"
        }
    });
    if (resp.status !== 201) {
        alert("Si è verificato un errore durante l'inserimento");
    }
    //Aggiorni la lista per avere l'ultimo
    updateTODOs();
};

//Quando la pagina carica
window.addEventListener("DOMContentLoaded", () => {
    //Aggiorno lista
    updateTODOs();
    document.getElementById("add-t-f").addEventListener("submit", async (event) => {
        //intercetta l'vento e blocca il form
        event.preventDefault();
        //ottengo la descrizione
        let description = document.getElementById("task_description");
        //Uso il metodo scritto più sopra per inserire il task
        await insertTODO(description.value);
        //Resetto il campo di testo dell'inserimento
        description.value = "";
    });
})}