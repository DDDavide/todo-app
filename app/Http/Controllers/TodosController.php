<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request; //accesso all'oggetto request che contiene i dati forniti da client
use Illuminate\Http\Response; //personalizzare il tipo di risposta http

class TodosController extends Controller
{
    /**
     * Questo controller gestisce la tabella todos
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    //Funziona gestione lista todos in JSON
    //chiamata con /api/todos [GET]
    public function list(){

        $results = app('db')->select("SELECT * FROM todos");
        return $results;
    }

    //crea un record todo nella tabella todos
    //chiamata con /api/todos [POST]
    public function create(Request $request){

        //recupero dei valori che l'utente passa in post
        $description = $request->input('description'); 
        $expireDate = $request->input('expireDate'); 

        if($expireDate){
            $results = app('db')->insert("     
                INSERT INTO `todos` ( `description`, `done`, `insertDate`, `expireDate`)
                VALUES ('$description', '0', now(), '$expireDate');"
            );
        }
        else{
            $results = app('db')->insert(" 
                INSERT INTO `todos` ( `description`, `done`, `insertDate`)
                VALUES ('$description', '0', now());
            ");
        }

        return new Response(null, 200);
    }



    //aggiorna un record todo nella tabella todos
    
    public function update(Request $request, $id){

        //recupero i valori che l'utente passa in put
        $description = $request->input('description'); 
        $done = $request->input('done');
        $expireDate = $request->input('expireDate'); 

        if($expireDate){ //se esiste expiredate
            $results = app('db')->update("     
                UPDATE todos
                SET
                    description = '$description',
                    done= $done,
                    expireDate' = '$expireDate'
                WHERE
                    id = $id;
            ");
        }
        else //in caso non esista
        {
            $results = app('db')->update("     
                UPDATE todos
                SET
                    description = '$description',
                    done= $done
                WHERE
                    id = $id;
            ");
        }

        return new Response(null, 200);
    }


    //elimina un record todo nella tabella todos
    
    public function delete(Request $request,$id)
    {
        $results = app('db')->delete("
            DELETE FROM todos WHERE id = $id
        ");
       // ritorno una risposta http
       return new Response(null, 204);
    }

}