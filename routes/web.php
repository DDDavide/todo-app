<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

//elenco dei todos in formato JSON [GET]
$router->get('/api/todos', 'TodosController@list');

//Crea un nuovo todo e lo inserisce nella tabella [POST]
$router->post('/api/todos', 'TodosController@create');

//Aggiorna un todo esistente passando l'id come parametro nell'url [PUT]
$router->put('/api/todos/{id}', 'TodosController@update');

//Elimina un  todo tramite l'id [DELETE]
$router->delete('/api/todos/{id}', 'TodosController@delete');